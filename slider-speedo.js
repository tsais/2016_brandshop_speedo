// JavaScript Document
/* written by john burrows 09/04/14 */
/* updated 06/12/13 */

/*HOW TO USE*/
/*add these two scripts to an html document:

<script src="http://www.sportsauthority.com/graphics/media/tsa/hp/template_2014/includes/jquery.min.js" type="text/javascript"></script>
<script src="slider-this-document-name.js" language="javascript" type="text/javascript"></script>


change "slider-this-document-name" to whatever you name this file
*/

var hostedURL = "http://img.ed4.net/tsa/images/Landing_Pages/2016/brandpages/speedo/";
//hostedURL="";
document.write('<script src="http://img.ed4.net/tsa/images/Landing_Pages/2014/hpReLaunch/jquery.slides.min.js" type="text/javascript"></script>');
document.write('<link rel="stylesheet" type="text/css" href="http://img.ed4.net/tsa/images/sliders/slider.css" />');




var $j=jQuery.noConflict();
var sliderDiv="#slider"; //id of the div that will be your slider container
var sliderWidth=526;
var sliderHeight=325;


var divContainer='<div id="slider" style="width:' + sliderWidth + 'px"></div>'
//document.write(divContainer);

//image file names
var slides = [];
slides.push("Major1.jpg");
slides.push("Major2.jpg");
slides.push("Major3.jpg");


//URLs for each image
var slideURLs = [];
slideURLs.push("http://www.sportsauthority.com/product/index.jsp?productId=24951806&amp;ab=MYOAS_CategoryPage_CatPageTop_BS_SpeedoLPQuantumSplice_030116_BRD_0_NA_LFS_NPCNS");
slideURLs.push("http://www.sportsauthority.com/Speedo-Mens-Fitness-Splice-Square-Leg-Swimsuit/product.jsp?productId=34362686&amp;ab=MYOAS_CategoryPage_CatPageTop_BS_SpeedoLPFitnessSplice_030116_BRD_0_NA_LFS_NPCNS");
slideURLs.push("http://www.sportsauthority.com/Seasonal-Apparel-Collections/October/Speedo-Vanquisher-Mirrored-Goggles/family.jsp?categoryId=81208576&amp;ab=MYOAS_CategoryPage_CatPageTop_BS_SpeedoLPMirroredGoggle_030116_BRD_0_NA_LFS_NPCNS");


var numberOfSlides = slides.length;

var navWidth=numberOfSlides*40;


function initializeNav(){
			
		//cycle through the plugin's pre-built nav to change it
		$j('.slidesjs-pagination-item a').each( function(index,element){
			
			// pagination item
			element = $j(element);
			
			// Number of the slide that corresponds to current pagination item
			var key = element.data().slidesjsItem;
			
			// Using that number get the slide div that corresponds to current pagination item
			var slide = $j("[slidesjs-index='" + key + "']");		
			
			//add my style for the <li> tag containing this <a> tag
			element.parent().addClass( "slider-button" );
			
			//Overwrite the default text with our tab copy
			//element.html(tab[index]);
			element.html('&bull;');
			
			//make the nav buttons center
			$j(sliderDiv+ ' ul').css({'width':navWidth});//'top':0,		
			
			
		});
		
		//position nav
			var startPos=$j(sliderDiv).position().top-$j(sliderDiv+ ' ul').position().top+sliderHeight;
			
			//set rollover to make nav appear			
			$j(sliderDiv).hover(function(){
			  $j(sliderDiv + ' ul').css({'top':startPos-35+'px'});
			  },function(){
			 $j(sliderDiv + ' ul').css({'top': startPos+'px'});
			});
}	
	
function buildSlider(){
	$j(sliderDiv).empty();
	
	for(var i=0;i<numberOfSlides;i++){		
			
		//add slide image
		$j(sliderDiv).append(' <div class="slide"><a href="' + slideURLs[i] + '"><img src="' + hostedURL + slides[i] + '" alt="" /></a></div>');				
			
	}
	$j(sliderDiv).addClass('first full left');
	
	//reset the force the slider height to the same as the images
	$j(sliderDiv).css({'min-height':sliderHeight});
	
	
}

//After everything on the page has loaded
$j(document).ready(function () {
	"use strict";
	buildSlider();
	

	//INITIAL SLIDER OPTIONS
	$j(function() {
      $j(sliderDiv).slidesjs({
		  width: sliderWidth,
		  height: sliderHeight,
		  
		  //previous/next buttons
		navigation: {
		  active: false,
		  effect: "slide"
		},
			  
		pagination: {
		  active: true,
		  effect: "slide"
		},
				  
		effect: {
			fade: {
			speed: 300
				},
			slide: {
			speed: 2000
				}
		},
		
		play: {
			// [boolean] Generate the play and stop buttons.
			// You cannot use your own buttons. Sorry.			
			active: false,
			
			// [string] Can be either "slide" or "fade".
			effect: "slide",
			
			// [number] Time spent on each slide in milliseconds.
			interval: 5000,
			
			// [boolean] Start playing the slideshow on load.
			auto: true,
			
			// [boolean] show/hide stop and play buttons
			swap: false,
			
			// [boolean] pause a playing slideshow on hover
			pauseOnHover: true,
			
			// [number] restart delay on inactive slideshow
			restartDelay: 10000
			},
			
		callback: {
			loaded: function(number) {
			
				//execute after slider is built
				initializeNav();
			},
			
			start: function(number) {
				// Do something awesome!
				// Passes slide number at start of animation
			},
			
			complete: function(number) {
				// Do something awesome!
				// Passes slide number at end of animation
			}
		}
      });
    });

	
	

});//end document ready